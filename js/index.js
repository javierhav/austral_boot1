$(function(){
    $("[data-toogle='tooltip']").tooltip();
    $("[data-toogle='popover']").popover();
    $('.carousel').carousel(
        {
            interval:5000
        });
    
    $('#contacto').on('show.bs.modal', function (e){
        console.log('el modal contacto se esta mostrando')

        $('#contactoBtn').removeClass('btn-outline-success');
        $('#contactoBtn').addClass('btn-dark');
        $('#contactoBtn').prop('disabled', true);

    });
    $('#contacto').on('shown.bs.modal', function (e){
        console.log('el modal contacto se esta mostro')
    });
    $('#contacto').on('hide.bs.modal', function (e){
        console.log('el modal contacto se esta ocultando')
    });
    $('#contacto').on('hidden.bs.modal', function (e){
        console.log('el modal ya contacto se oculto')
        $('#contactoBtn').prop('disabled', false);
        $('#contactoBtn').removeClass('btn-dark');
        $('#contactoBtn').addClass('btn-outline-success');
    });

});